defmodule Pmail.Emailer do
  use Pmail.Web, :model

  schema "" do
    field :first, :string, virtual: true
    field :last, :string, virtual: true
    field :domain, :string, virtual: true
  end

  @required_fields ~w(first last domain)
  @optional_fields ~w()

  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
    #|> validate_length(:body, min: 5) - any validations, etc.
  end
end
