defmodule Pmail.PageController do
  use Pmail.Web, :controller

  def index(conn, _params) do
    changeset = Pmail.Emailer.changeset(%Pmail.Emailer{})
    render conn, "index.html", changeset: changeset
  end

  def results(conn, %{"emailer" => emailer_params}) do
    first = emailer_params["first"]
    last = emailer_params["last"]
    domain = emailer_params["domain"]
    emails_find = check_emails(emailer_params)
    render conn, "results.html", first: first, last: last, domain: domain, emails_find: emails_find
  end


  def crate_pattern_list(emailer_params) do
    first_lower = String.downcase(emailer_params["first"])
    last_lower = String.downcase(emailer_params["last"])
    domain = emailer_params["domain"]
    first_list = String.codepoints(first_lower)
    last_list = String.codepoints(last_lower)
    ff = List.first(first_list)
    fl = List.first(last_list)
    base_list  = []

    # patterns
    base_list = List.insert_at(base_list, 0, "#{first_lower}#{last_lower}@#{domain}")
    base_list = List.insert_at(base_list, 1, "#{first_lower}.#{last_lower}@#{domain}")
    base_list = List.insert_at(base_list, 2, "#{ff}#{last_lower}@#{domain}")
    base_list = List.insert_at(base_list, 3, "#{ff}.#{last_lower}@#{domain}")
    base_list = List.insert_at(base_list, 4, "#{first_lower}#{fl}@#{domain}")
    base_list = List.insert_at(base_list, 5, "#{first_lower}.#{fl}@#{domain}")

    base_list = List.insert_at(base_list, 6, "#{last_lower}#{first_lower}@#{domain}")
    base_list = List.insert_at(base_list, 7, "#{last_lower}.#{first_lower}@#{domain}")
    base_list = List.insert_at(base_list, 8, "#{last_lower}#{ff}@#{domain}")
    base_list = List.insert_at(base_list, 9, "#{last_lower}.#{ff}@#{domain}")
    base_list = List.insert_at(base_list, 10, "#{fl}#{first_lower}@#{domain}")
    base_list = List.insert_at(base_list, 11, "#{fl}.#{first_lower}@#{domain}")

    base_list = List.insert_at(base_list, 12, "#{first_lower}@#{domain}")
    base_list = List.insert_at(base_list, 13, "#{last_lower}@#{domain}")

    base_list
  end

  def check_emails(emailer_params) do
    pattern_list = crate_pattern_list(emailer_params)
    correct_email = []
    check_email(pattern_list, correct_email)
  end

  def check_email([head|tail], correct_email) do
    if EmailChecker.valid?(head) do
      correct_email = List.insert_at(correct_email, 0, head)
    end
    check_email(tail, correct_email)
  end

  def check_email([], correct_email) do
    correct_email
  end
end
