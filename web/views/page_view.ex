defmodule Pmail.PageView do
  use Pmail.Web, :view
  def csrf_token(conn) do
    Map.get(conn.req_cookies, "_csrf_token")
  end
end
