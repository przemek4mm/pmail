ExUnit.start

Mix.Task.run "ecto.create", ~w(-r Pmail.Repo --quiet)
Mix.Task.run "ecto.migrate", ~w(-r Pmail.Repo --quiet)
Ecto.Adapters.SQL.begin_test_transaction(Pmail.Repo)

